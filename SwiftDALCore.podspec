Pod::Spec.new do |s|

# 1
s.platform = :ios
s.ios.deployment_target = '13.0'
s.name = "SwiftDALCore"
s.summary = "SwiftDalCore lets a user access data layer."
s.requires_arc = true

# 2
s.version = "1.0.0"

# 3
s.license = {
    type: "MIT",
    text: File.open("LICENSE").read
  }

# 4 - Replace with your name and e-mail address
s.author = { "Mohanasundaram" => "mohanasundaram.flairlabs.ext.idfy.com" }

# 5 - Replace this URL with your own GitHub page's URL (from the address bar)
s.homepage = "https://gitlab.com/mohanasundaram.flairlabs/SwiftDalCore"

# 6 - Replace this URL with your own Git URL from "Quick Setup"
s.source = { :git => "https://gitlab.com/mohanasundaram.flairlabs/SwiftDalCore.git", 
             :tag => "#{s.version}" }

# 7
s.framework = "UIKit"

# 8
s.source_files = "SwiftDalCore/**/*.{swift}"

# 9
s.resources = "SwiftDalCore/**/*.{png,jpeg,jpg,storyboard,xib,xcassets}"

# 10
s.swift_version = "4.2"

#11
s.dependency 'SwiftPhoenixClient'
s.dependency 'Alamofire', '~> 5.4'
s.dependency 'AlamofireImage'
s.dependency 'Starscream', '~> 4.0.0'
s.dependency 'lottie-ios'
s.dependency 'Cleanse'
s.dependency 'RxSwift', '6.5.0'

end
