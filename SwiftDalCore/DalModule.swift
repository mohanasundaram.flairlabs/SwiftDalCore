//
//  DalModule.swift
//  captureapp
//
//  Created by Admin on 29/03/22.
//

import Foundation
import Cleanse

public typealias SingletonBinder = Binder<Singleton>

public struct DalModule : Module {

    private var dalConfig = DalConfig()
    
    init(dalConfig:DalConfig) {
        self.dalConfig = dalConfig
    }
    public func provideDalConfig() -> DalConfig {
        return dalConfig
    }
    public static func configure(binder: SingletonBinder) {
        binder
            .bind(DalConfig.self)
            .sharedInScope()
            .to (factory: DalConfig.init)

//        binder
//            .bind(DALCapture.self)
//            .sharedInScope()
//            .to { (dalConfig: DalConfig, loggerWrapper: LoggerWrapper) in
//                return DALCapture(loggerWrapper: loggerWrapper, dalConfig: dalConfig)
//            }
    }
}
