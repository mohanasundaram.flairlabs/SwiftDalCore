//
//  Artifacts.swift
//  Hostapp
//
//  Created by Admin on 28/02/22.
//

import Foundation

public class Artifacts {
    var type:String = ""
    var taskKey:String = ""
    var taskType:String = ""
    var documentType:String = ""
    var artifactKey:String = ""
    var side:String = ""
    var value:String = ""
    var templateId:Int = 0
    var optionList:[String] = []

    public init(artifactKey:String) {
        self.artifactKey = artifactKey
    }
    public init(templateId:Int,type:String,taskType:String,taskKey:String,documentType:String,artifactKey:String,side:String,value:String,optionList:[String]) {
        self.templateId = templateId
        self.type = type
        self.taskKey = taskKey
        self.taskKey = taskKey
        self.documentType = documentType
        self.artifactKey = artifactKey
        self.side = side
        self.value = value
        self.optionList = optionList
    }
    public func getType() -> String {
        return type
    }
    public func setType(type:String) {
        self.type = type
    }
    public func getDocumentType() -> String {
        return documentType
    }
    public func setDocumentType(documentType:String) {
        self.documentType = documentType
    }
    public func getArtifactKey() -> String {
        return artifactKey
    }
    public func setArtifactKey(artifactKey:String) {
        self.artifactKey = artifactKey
    }
    public func getValue() -> String {
        return value
    }
    public func setValue(value:String) {
        self.value = value
    }
    public func getTaskKey() -> String {
        return taskKey
    }
    public func setTaskKey(taskKey:String) {
        self.taskKey = taskKey
    }
    public func getTaskType() -> String {
        return taskType
    }
    public func setTaskType(taskType:String) {
        self.taskType = taskType
    }
    public func getSide() -> String {
        return side
    }
    public func setSide(side:String) {
        self.side = side
    }
    public func getOptionList() -> [String] {
        return optionList
    }
    public func setOptionList(optionList:[String]) {
        self.optionList = optionList
    }
    public func getTemplateId() -> Int {
        return templateId
    }
    public func setTemplateId(templateId:Int) {
        self.templateId = templateId
    }
}
