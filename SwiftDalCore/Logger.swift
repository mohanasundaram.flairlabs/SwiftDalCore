//
//  Logger.swift
//  app
//
//  Created by Admin on 29/11/21.
//

import Foundation
import CoreMedia
import UIKit
import CoreData

public class Logger: NSObject {
    
    var token = ""
    
    var loggerUrl = "https://capture.kyc.idfystaging.com/js-logger/publish"
    
    var defaultLogDetails: [String: Any] = [:]
    
    var metaDefaults: [String: Any] = [:]
    
    var logSequenceCtr = 0
    
    var loglevel = LogLevel()
    
    var logger_session_id = UUID().uuidString

    var logsHandlerInstance: LogsHandler?

    var isDBInstanceCreated: Bool = false
    private var enableLogging: Bool = true // by default logging is enabled
    var isRetryEnabled: Bool = true // in default retry is enabled
    
    //    var loggerCapture = LoggerWrapper.sharedInstance.getLoggers(key: "capture")
    //    var loggerHCheck = LoggerWrapper.sharedInstance.getLoggers(key: "healthCheck")
    //    var loggerPg = LoggerWrapper.sharedInstance.getLoggers(key: "pg")
    //    var loggerVkyc = LoggerWrapper.sharedInstance.getLoggers(key: "vkyc")


//    init(context:NSPersistentContainer) {
//        self.context = context
//    }
    public override init() {
        super.init()
    }
    public func createInstance(token: String, loggerUrl: String) {
        self.token = token
//        self.loggerUrl = loggerUrl

        // if retry is enabled and db instance is not created initiate db
        if self.isRetryEnabled && !isDBInstanceCreated {
            initiateDb()
        }
    }
    public func createInstance(token: String, loggerUrl: String, enableLogging: Bool, isRetryEnabled: Bool) {
        self.token = token
//        self.loggerUrl = loggerUrl

        // if retry is enabled and db instance is not created initiate db
        if self.isRetryEnabled && !isDBInstanceCreated {
            initiateDb()
        }
    }
    private func initiateDb() {

        if logsHandlerInstance == nil {
            logsHandlerInstance = LogsHandler()
            isDBInstanceCreated = true
        }
    }

    public func clearDbEntries() {
        if logsHandlerInstance != nil {
            logsHandlerInstance?.clearEntries()
        }
    }
    public func setDefaults(defaultLogDetails: ModalLogDetails) {
        for case let (label?, value) in Mirror(reflecting: defaultLogDetails)
                .children.map({ ($0.label, $0.value) }) {
            if value as? String != "" {
                self.defaultLogDetails.updateValue(value, forKey: label)
            }
        }
    }

    public func setMetaDefaults(metaDefaults: [String: Any]) {
        self.metaDefaults = metaDefaults
    }
    public func log(logLevel: String, logDetails: ModalLogDetails, meta: [String: Any]) {
        var logs: [String: Any] = [:]
        var metalogs: [String: Any] = [:]
        
        logs = defaultLogDetails
        logs.updateValue(logLevel, forKey: "log_level")
        logs.updateValue(Date.currentTimeStamp, forKey: "timestamp")
        logs.updateValue(logger_session_id, forKey: "logger_session_id")
        
        if metaDefaults.count > 0 {
            metalogs = metaDefaults
        }
        
        if meta.count > 0 {
            metalogs =  metalogs.merging(meta) { (_, new) in new }
        }
        metalogs.updateValue(logSequenceCtr + 1, forKey: "logSequenceNo")
        metalogs.updateValue(false, forKey: "retransmitted")
        
        for case let (label?, value) in Mirror(reflecting: logDetails)
                .children.map({ ($0.label, $0.value) }) {
            
            if value as? String != "" || label == "event_name" {
                logs.updateValue(value, forKey: label)
            }
        }
        logs.updateValue(metalogs, forKey: "details")
        
        if logs["liveMonitoring"] as! Bool == false {
            logs.removeValue(forKey: "liveMonitoring")
        }

        let urls = loggerUrl + "?t=" + token

        do {
            let jsonData = try JSONSerialization.data(withJSONObject: logs, options: .prettyPrinted)
            DispatchQueue.main.async {
            ApiController.logger(controller: UIViewController(), info: jsonData, url: urls) { success, fail in
                if success != nil {
//                    if self.getLogsRetention() {
//                        logsHandlerInstance?.retryEntries(dalCapture: dalCapture, token: token, loggerUrl: loggerUrl)
//                    }
                } else {
//                    if fail?.value(forKey: "status") as! Int >= 500 {
//                        if getLogsRetention() {
//                            logsHandlerInstance?.addEntry(entry: logs)
//                        }
//                    } else {
//                        if getLogsRetention() {
//                            logsHandlerInstance?.addEntry(entry: logs)
//                        }
//                    }
                }
            }
            }

        } catch {
            print(error.localizedDescription)
        }
    }
    public func logPageRender(loggerStartTime: Double, component: String,referenceId:String,referenceType:String,meta: [String: Any]) {
//        let referenceId = 

        var dataRenderTat = ModalLogDetails()
        dataRenderTat.service_category = "CaptureSDK"
        dataRenderTat.service = "Render"
        dataRenderTat.event_type = "Render.TAT"
        dataRenderTat.event_name = getTatSince(start: loggerStartTime)
        dataRenderTat.component = component
        dataRenderTat.event_source = "componentDidMount"
        dataRenderTat.logger_session_id = logger_session_id
        dataRenderTat.reference_id = referenceId
        dataRenderTat.reference_type = referenceType // dalCapture.getRequestId() != "" ? "AV.TaskID" : "CaptureID"

        log(logLevel: LogLevel.shared.Info, logDetails: dataRenderTat, meta: meta)

        var dataPageRender = ModalLogDetails()
        dataPageRender.service_category = "CaptureSDK"
        dataPageRender.service = "Render"
        dataPageRender.event_type = "RenderPage"
        dataPageRender.event_name = component
        dataPageRender.component = "LoggerWrapper"
        dataPageRender.event_source = "logPageRender"
        dataPageRender.logger_session_id = logger_session_id
        dataPageRender.reference_id = referenceId
        dataPageRender.reference_type = referenceType // dalCapture.getRequestId() != "" ? "AV.TaskID" : "CaptureID"

        log(logLevel: LogLevel.shared.Info, logDetails: dataPageRender, meta: meta)
        
    }
    public func logPageVisit(pageName: String, pageComponent: String, source: String) {
            var dataPageVisit = ModalLogDetails()
            dataPageVisit.service_category = "CaptureSDK"
            dataPageVisit.service = "Routing"
            dataPageVisit.event_type = "Landed"
            dataPageVisit.event_name = pageName
            dataPageVisit.component = pageComponent
            dataPageVisit.log_version = "v1"
            dataPageVisit.publish_to_dlk = true
            dataPageVisit.logger_session_id = logger_session_id
            if source != "" && source.trimmingCharacters(in: .whitespacesAndNewlines)  != "" {
                dataPageVisit.event_source = source
            } else {
                dataPageVisit.event_source = "componentDidMount"
            }
            log(logLevel: LogLevel.shared.Info, logDetails: dataPageVisit, meta: [:])
    }
    private func getLogsRetention() -> Bool {
           if logsHandlerInstance != nil && isRetryEnabled {
               return true
           } else {
               return false
           }
       }
    func getTatSince(start: Double) -> String {
        let tat = NSDate().timeIntervalSince1970 * 1000 - start
        return String(tat)
    }
}

