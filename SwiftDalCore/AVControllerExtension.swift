//
//  AVControllerExtension.swift
//  app
//
//  Created by Admin on 28/10/21.
//

import Foundation
import UIKit
import CoreLocation

public func convert(cmage: CIImage) -> UIImage {
     let context = CIContext(options: nil)
     let cgImage = context.createCGImage(cmage, from: cmage.extent)!
     let image = UIImage(cgImage: cgImage)
     return image
}
public func generateImageWithText(text: String, image: UIImage) -> UIImage? {
    
    let imageView = UIImageView(image: image)
//        imageView.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi/2))
    imageView.backgroundColor = UIColor.clear
    imageView.frame = CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height)
    let view = UIView(frame: CGRect(x: 0, y: 0, width: 300, height: 90))
    let textField = UITextField(frame: CGRect(x: 20, y: 10, width: UIScreen.main.bounds.width, height: 20))
    textField.backgroundColor = .clear
    textField.textColor = .white
    textField.text = "\(getCurrentShortDate())"
    textField.textAlignment = .left
    let textField1 = UITextField(frame: CGRect(x: 20, y: 30, width: UIScreen.main.bounds.width, height: 20))
    textField1.backgroundColor = .clear
    textField1.textColor = .white
    textField1.text = "\(getCurrentShortTime())"
    textField1.textAlignment = .left
    let textField2 = UITextField(frame: CGRect(x: 20, y: 50, width: UIScreen.main.bounds.width, height: 20))
    textField2.backgroundColor = .clear
    textField2.textColor = .white
    textField2.text = "Lat,Long"
    textField.textAlignment = .left
    let textField3 = UITextField(frame: CGRect(x: 20, y: 70, width: UIScreen.main.bounds.width, height: 20))
    textField3.backgroundColor = .clear
    textField3.textColor = .white
    if let location = LocationManager.sharedInstance.currentLocation {
        textField3.text = "\(location.coordinate.latitude.round(to: 5)),\(location.coordinate.longitude.round(to: 5) )"
    }
    textField2.textAlignment = .left
    view.addSubview(textField)
    view.addSubview(textField1)
    view.addSubview(textField2)
    view.addSubview(textField3)
//    view.transform = CGAffineTransform(rotationAngle: -CGFloat(Double.pi/2))
    UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, 0)
    imageView.layer.render(in: UIGraphicsGetCurrentContext()!)
    view.layer.render(in: UIGraphicsGetCurrentContext()!)
    let imageWithText = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()

    return imageWithText
}
public func buffer(from image: UIImage) -> CVPixelBuffer? {
  let attrs = [kCVPixelBufferCGImageCompatibilityKey: kCFBooleanTrue, kCVPixelBufferCGBitmapContextCompatibilityKey: kCFBooleanTrue] as CFDictionary
  var pixelBuffer: CVPixelBuffer?
  let status = CVPixelBufferCreate(kCFAllocatorDefault, Int(image.size.width), Int(image.size.height), kCVPixelFormatType_32ARGB, attrs, &pixelBuffer)
  guard status == kCVReturnSuccess else {
    return nil
  }

  CVPixelBufferLockBaseAddress(pixelBuffer!, CVPixelBufferLockFlags(rawValue: 0))
  let pixelData = CVPixelBufferGetBaseAddress(pixelBuffer!)

  let rgbColorSpace = CGColorSpaceCreateDeviceRGB()
  let context = CGContext(data: pixelData, width: Int(image.size.width), height: Int(image.size.height), bitsPerComponent: 8, bytesPerRow: CVPixelBufferGetBytesPerRow(pixelBuffer!), space: rgbColorSpace, bitmapInfo: CGImageAlphaInfo.noneSkipFirst.rawValue)

  context?.translateBy(x: 0, y: image.size.height)
  context?.scaleBy(x: 1.0, y: -1.0)

  UIGraphicsPushContext(context!)
  image.draw(in: CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height))
  UIGraphicsPopContext()
  CVPixelBufferUnlockBaseAddress(pixelBuffer!, CVPixelBufferLockFlags(rawValue: 0))

  return pixelBuffer
}
extension Double {
    public func round(to places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return Darwin.round(self * divisor) / divisor
    }
}
