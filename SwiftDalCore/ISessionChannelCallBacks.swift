//
//  ISessionChannelCallBacks.swift
//  core
//
//  Created by Admin on 18/04/22.
//

import Foundation

public protocol ISessionChannel {
    func onSessionChannelSuccess(object:NSObject)
    func onSessionChannelFailure(message:String)
}

