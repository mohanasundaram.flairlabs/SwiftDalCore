//
//  EnumLogger.swift
//  svc library
//
//  Created by Admin on 01/04/22.
//

import Foundation
public enum EnumLogger:String {
    case CAPTURE = "capture"
    case HEALTHCHECK = "healthcheck"
    case VC = "vc"
    case PG = "pg"
    case MSADAPTOR = "msadaptor"
}
