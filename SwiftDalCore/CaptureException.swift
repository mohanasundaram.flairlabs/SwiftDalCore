//
//  CaptureException.swift
//  app
//
//  Created by Admin on 23/12/21.
//

import Foundation
enum CaptureException: Error {
    case errorMessage(String)
}
