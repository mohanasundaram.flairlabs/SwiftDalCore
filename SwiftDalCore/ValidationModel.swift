//
//  ValidationModel.swift
//  core
//
//  Created by FL Mohan on 10/05/22.
//

import Foundation

public class ValidationModel {
    var validations = ""

    public init(validations:String) {
        self.validations = validations
    }

    public func getValidations() -> String {
        return validations
    }

    public func setValidations(validations:String) {
        self.validations = validations
    }
}
