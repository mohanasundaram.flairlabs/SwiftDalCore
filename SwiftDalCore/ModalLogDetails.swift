//
//  ModalLogDetails.swift
//  app
//
//  Created by Admin on 29/11/21.
//

import Foundation

public struct ModalLogDetails: Codable {
    public init(service_category: String = "", service: String = "", timestamp: String = "", event_type: String = "", event_name: String = "", component: String = "", event_source: String = "", logger_session_id: String = "", exceptionName: String? = "", exceptionDescription: String? = "", reference_id: String? = "", reference_type: String? = "", log_version: String? = "", app_vsn: String = "", liveMonitoring: Bool? = false, publish_to_dlk: Bool? = false) {
        self.service_category = service_category
        self.service = service
        self.timestamp = timestamp
        self.event_type = event_type
        self.event_name = event_name
        self.component = component
        self.event_source = event_source
        self.logger_session_id = logger_session_id
        self.exceptionName = exceptionName
        self.exceptionDescription = exceptionDescription
        self.reference_id = reference_id
        self.reference_type = reference_type
        self.log_version = log_version
        self.app_vsn = app_vsn
        self.liveMonitoring = liveMonitoring
        self.publish_to_dlk = publish_to_dlk
    }

    public var service_category: String = ""
    public var service: String = ""
    public var timestamp: String = ""
    public var event_type: String = ""
    public var event_name: String = ""
    public var component: String = ""
    public var event_source: String = ""
    public var logger_session_id: String = ""
    public var exceptionName: String? = ""
    public var exceptionDescription: String? = ""
    public var reference_id: String? = ""
    public var reference_type: String? = ""
    public var log_version: String? = ""
    public var app_vsn = ""

    public var liveMonitoring: Bool? = false
    public var publish_to_dlk: Bool? = false
}
