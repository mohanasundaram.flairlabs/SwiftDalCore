//
//  Extension.swift
//  app
//
//  Created by Admin on 11/09/21.
//

import Foundation
import UIKit
import AVFoundation
import AlamofireImage

extension UIApplication {
    
    func getTopViewController() -> UIViewController {
        
        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        
        if var topController = keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            return topController
        }
        return UIViewController()
    }
}
extension Encodable {
    
    public var dictionary: Any? {
        guard let data = try? JSONEncoder().encode(self) else { return nil }
        return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] }
    }
}

public func currentTimeInMiliseconds() -> Int! {
            let currentDate = NSDate()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
            let date = dateFormatter.date(from: dateFormatter.string(from: currentDate as Date))
            let nowDouble = date!.timeIntervalSince1970
            return Int(nowDouble*1000000000)
        }
public func currentDate() -> String {
   let dateFormatterGet = DateFormatter()
   dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
   let myString = dateFormatterGet.string(from: Date())
   return myString
}
public func getCurrentShortDate() -> String {
    let todaysDate = NSDate()
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd/MM/yyyy"
    let DateInFormat = dateFormatter.string(from: todaysDate as Date)
    return DateInFormat
}
public func getCurrentShortTime() -> String {
    let todaysDate = NSDate()
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "HH:mm:ss"
    let DateInFormat = dateFormatter.string(from: todaysDate as Date)
    return DateInFormat
}
public func getCurrentShortTimewithAP() -> String {
    let todaysDate = NSDate()
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "HH:mm:ss a"
    let DateInFormat = dateFormatter.string(from: todaysDate as Date)
    return DateInFormat
}
public extension Dictionary {
  func contains(key: Key) -> Bool {
    self.index(forKey: key) != nil
  }
}
public extension Data {
    mutating func append(_ string: String, using encoding: String.Encoding = .utf8) {
        if let data = string.data(using: encoding) {
            append(data)
        }
    }
}
public extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {

            let content = try NSMutableAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
            content.addAttributes([NSAttributedString.Key.foregroundColor: UIColor.white],range: NSMakeRange(0, content.length))
            return content
        } catch {
            return nil
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    func addingDashes() -> String {

        var result = ""

        for (offset, character) in self.enumerated() {
            if offset != 0 && offset % 1 == 0 {
                result.append("-")
            }

            result.append(character)
        }
        return result
    }

    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}
public func capitalizingEachWord(sentenceToCap:String) -> String {
    var label = sentenceToCap
    label = label.replacingOccurrences(of: "_", with: " ")
    let breakupSentence = label.components(separatedBy: " ")
    var newSentence = ""
    for wordInSentence  in breakupSentence {
        newSentence = "\(newSentence) \(wordInSentence.capitalized)"
    }
    return newSentence
}

public func isValidInput(regex:String,Input:String) -> Bool {
    let RegEx = regex
    let Test = NSPredicate(format:"SELF MATCHES %@", RegEx)
    return Test.evaluate(with: Input)
}
public func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
    let size = image.size
    
    let widthRatio  = targetSize.width  / size.width
    let heightRatio = targetSize.height / size.height
    
    // Figure out what our orientation is, and use that to form the rectangle
    var newSize: CGSize
    if widthRatio > heightRatio {
        newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
    } else {
        newSize = CGSize(width: size.width * widthRatio, height: size.height * widthRatio)
    }
    
    // This is the rect that we've calculated out and this is what is actually used below
    let rect = CGRect(origin: .zero, size: newSize)
    
    // Actually do the resizing to the rect using the ImageContext stuff
    UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
    image.draw(in: rect)
    guard let newImage = UIGraphicsGetImageFromCurrentImageContext() else { return UIImage() }
    UIGraphicsEndImageContext()
    
    return newImage
}
//extension UIScreen {

    public func widthOfSafeArea() -> CGFloat {

        guard let rootView = UIApplication.shared.windows.first else { return 0 }

        if #available(iOS 11.0, *) {

            let leftInset = rootView.safeAreaInsets.left

            let rightInset = rootView.safeAreaInsets.right

            return rootView.bounds.width - leftInset - rightInset

        } else {

            return rootView.bounds.width

        }

    }

    public func heightOfSafeArea() -> CGFloat {

        guard let rootView = UIApplication.shared.windows.first else { return 0 }

        if #available(iOS 11.0, *) {

//            let topInset = rootView.safeAreaInsets.top

            let bottomInset = rootView.safeAreaInsets.bottom

            return rootView.bounds.height - bottomInset

        } else {

            return rootView.bounds.height

        }

    }
public func getFrameSize() -> CGRect {
    guard let rootView = UIApplication.shared.windows.first else { return CGRect(x: 0,y: 0,width: 0,height: 0) }

    let bottomInset = rootView.safeAreaInsets.bottom

    return CGRect(x: 0, y: 44, width: rootView.bounds.width, height: rootView.bounds.height - bottomInset)
}

//}
extension UIViewController {
    public func checkforCameraAccess(completion:@escaping (_ success: Bool?) -> Void) {
        if AVCaptureDevice.authorizationStatus(for: AVMediaType.video) ==  AVAuthorizationStatus.authorized {
            completion(true)
        } else {
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted: Bool) -> Void in
                if granted == true {
                    completion(true)
                } else {
                    DispatchQueue.main.async {
                        self.showAlertForCameraAccess(completion: { success in
                            if success == true {
                                self.checkforCameraAccess { success in }
                            } else {
                                completion(false)
                            }
                        })
                    }
                }
            })
        }
    }
    public func showAlert(title: String, message: String?, actionTitle: String, completion:@escaping (_ success: Bool?) -> Void) {
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: actionTitle, style: .default, handler: {_ in
            completion(true)
        }))
        self.present(alertController, animated: true, completion: nil)
    }
    public func showAlert(title: String, message: String?, actionTitle1: String?, actionTitle2: String?, completion:@escaping (_ success: Bool?) -> Void) {
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: actionTitle1, style: .destructive, handler: { _ in
             completion(true)
         }))
        alertController.addAction(UIAlertAction(title: actionTitle2, style: .default, handler: {_ in }))
      
        self.present(alertController, animated: true, completion: nil)
    }
    public func showAlertOnMessage(title: String, message: String, completion: @escaping (_ success: Bool) -> (Void)) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            self.present(alert, animated: true, completion: nil)
            let when = DispatchTime.now() + 2
            DispatchQueue.main.asyncAfter(deadline: when){
              alert.dismiss(animated: true, completion: nil)
              completion(true)
            }
    }
    public func showAlertonNoInternet(title: String = "No Internet Connection.", message: String = "Your data connection appears to be weak or unavailable. Please check that you have a data connection and try again.") {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action1 = UIAlertAction(title: "Settings", style: .cancel) { (_: UIAlertAction) in
            print("Go to Settings Page")
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {  return }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                })
            }
        }
        let action2 = UIAlertAction(title: "Close", style: .default) { (_: UIAlertAction) in
            print("You've pressed Cancel")
        }
        alertController.addAction(action1)
        alertController.addAction(action2)
        self.present(alertController, animated: true, completion: nil)
    }
    public func showAlertForLocationAccess(completion:@escaping (_ success: Bool?) -> Void) {
        let alertController = UIAlertController(title: "Enable Location Services for better results", message: "Do you want to enable Location Service for this app?", preferredStyle: .alert)
        let action1 = UIAlertAction(title: "Settings", style: .cancel) { (_: UIAlertAction) in
            print("Go to Settings Page")
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {  return }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                    completion(true)
                })
            }
        }
        let action2 = UIAlertAction(title: "Close", style: .default) { (_: UIAlertAction) in
            print("You've pressed Cancel")
            completion(false)
        }
        alertController.addAction(action1)
        alertController.addAction(action2)
        self.present(alertController, animated: true, completion: nil)
    }
    public func showAlertForCameraAccess(title: String = "Enable Camera Services for Image Capture / Video call", message: String = "Do you want to enable Camera Service for this app?", completion:@escaping (_ success: Bool?) -> Void) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action1 = UIAlertAction(title: "Settings", style: .cancel) { (_: UIAlertAction) in
            print("Go to Settings Page")
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {  return }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                    completion(true)
                })
            }
        }
        let action2 = UIAlertAction(title: "Close", style: .default) { (_: UIAlertAction) in
            print("You've pressed Cancel")
            completion(false)
        }
        alertController.addAction(action1)
        alertController.addAction(action2)
        self.present(alertController, animated: true, completion: nil)
    }
    public func showAlertForMicroPhoneAccess(title: String = "Enable Microphone Services for video call", message: String = "Do you want to enable Microphone Service for this app?", completion:@escaping (_ success: Bool?) -> Void) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action1 = UIAlertAction(title: "Settings", style: .cancel) { (_: UIAlertAction) in
            print("Go to Settings Page")
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {  return }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                    completion(true)
                })
            }
        }
        let action2 = UIAlertAction(title: "Close", style: .default) { (_: UIAlertAction) in
            print("You've pressed Cancel")
            completion(false)
        }
        alertController.addAction(action1)
        alertController.addAction(action2)
        self.present(alertController, animated: true, completion: nil)
    }
}
extension String {
    fileprivate func indentingNewlines(by spaceCount: Int = 4) -> String {
        let spaces = String(repeating: " ", count: spaceCount)
        return replacingOccurrences(of: "\n", with: "\n\(spaces)")
    }
    public func startcased() -> String {
        components(separatedBy: " ")
            .map { $0.prefix(1).uppercased() + $0.dropFirst() }
            .joined(separator: " ")
    }
    
}
extension CharacterSet {
    static let urlQueryValueAllowed: CharacterSet = {
        let generalDelimitersToEncode = ":#[]@" // does not include "?" or "/" due to RFC 3986 - Section 3.4
        let subDelimitersToEncode = "!$&'()*+,;="

        var allowed = CharacterSet.urlQueryAllowed
        allowed.remove(charactersIn: generalDelimitersToEncode + subDelimitersToEncode)
        return allowed
    }()
}
extension UIView {
   public func fixInView(_ container: UIView!) {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.frame = container.frame
        container.addSubview(self)
        NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: container, attribute: .leading, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: container, attribute: .trailing, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: container, attribute: .top, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: container, attribute: .bottom, multiplier: 1.0, constant: 0).isActive = true
    }
}
public func hexStringToUIColor (hex: String) -> UIColor {
    var cString: String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    
    if cString.hasPrefix("#") {
        cString.remove(at: cString.startIndex)
    }
    
    if (cString.count) != 6 {
        return UIColor.gray
    }
    
    var rgbValue: UInt64 = 0
    Scanner(string: cString).scanHexInt64(&rgbValue)
    
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}
extension UIView {
    public func addDashedBorder(view:UIView,color:UIColor) {
        let shapeLayer:CAShapeLayer = CAShapeLayer()
        let frameSize = view.frame.size
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)

        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color.cgColor
        shapeLayer.lineWidth = 2
        shapeLayer.lineJoin = CAShapeLayerLineJoin.round
        shapeLayer.lineDashPattern = [6,3]
        shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 0).cgPath

        view.layer.addSublayer(shapeLayer)
    }
    public func setCornerBorder(color: UIColor? = nil, cornerRadius: CGFloat = 15.0, borderWidth: CGFloat = 1.5) {
        self.layer.borderColor = color != nil ? color!.cgColor : UIColor.clear.cgColor
        self.layer.borderWidth = borderWidth
        self.layer.cornerRadius = cornerRadius
        self.clipsToBounds = true
    }
    public func setAsShadow(bounds: CGRect, cornerRadius: CGFloat = 0.0, shadowRadius: CGFloat = 1) {
        self.backgroundColor = UIColor.clear
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath
        self.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        self.layer.shadowOpacity = 0.7
        self.layer.shadowRadius = shadowRadius
        self.layer.masksToBounds = true
        self.clipsToBounds = false
    }
    public func removeSubviews() {
        self.subviews.forEach {
            $0.removeFromSuperview()
        }
    }
}
extension UIImage {
    public func cropToRect(rect: CGRect!) -> UIImage? {
        // Correct rect size based on the device screen scale
        let scaledRect = CGRect(x: rect.origin.x * self.scale, y: rect.origin.y * self.scale, width: rect.size.width * self.scale, height: rect.size.height * self.scale);
        // New CGImage reference based on the input image (self) and the specified rect
        let imageRef = self.cgImage?.cropping(to: scaledRect)
        // Gets an UIImage from the CGImage
        let result = UIImage(cgImage: imageRef!, scale: self.scale, orientation: self.imageOrientation)
        // Returns the final image, or NULL on error
        return result;
    }
     public func croppedInOVerlay(rect: CGRect) -> UIImage {
        func rad(_ degree: Double) -> CGFloat {
            return CGFloat(degree / 180.0 * .pi)
        }

        var rectTransform: CGAffineTransform
        switch imageOrientation {
        case .left:
            rectTransform = CGAffineTransform(rotationAngle: rad(90)).translatedBy(x: 0, y: -self.size.height)
        case .right:
            rectTransform = CGAffineTransform(rotationAngle: rad(-90)).translatedBy(x: -self.size.width, y: 0)
        case .down:
            rectTransform = CGAffineTransform(rotationAngle: rad(-180)).translatedBy(x: -self.size.width, y: -self.size.height)
        default:
            rectTransform = .identity
        }
        rectTransform = rectTransform.scaledBy(x: self.scale, y: self.scale)

        let imageRef = self.cgImage!.cropping(to: rect.applying(rectTransform))
        let result = UIImage(cgImage: imageRef!, scale: self.scale, orientation: self.imageOrientation)
        return result
    }
}
extension UIImage {
    public static func loadFrom(url: URL, completion: @escaping (_ image: UIImage?) -> ()) {
        DispatchQueue.global().async {
            if let data = try? Data(contentsOf: url) {
                DispatchQueue.main.async {
                    completion(UIImage(data: data))
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil)
                }
            }
        }
    }
}
extension UIImageView {
    public func downloadImage(from url : String?, completion: ((_ success: Bool?) -> Void)?){
        self.image = nil
        guard let url = URL(string: url ?? "") else { return print("invalid URL for image")}
        self.af.setImage(
            withURL: url,
            placeholderImage: nil,
            filter: nil,
            completion: { response in
                completion?(true)
            }
        )
//        guard let stringURL = url, let url = URL(string: stringURL) else { return }
//        func setImage(image:UIImage?) {
//            DispatchQueue.main.async {
//                self.image = image
//            }
//        }
//        if let data = try? Data(contentsOf: url), let image = UIImage(data: data) {
//            DispatchQueue.main.async {
//                setImage(image: image)
//                completion?(true)
//            }
//        } else {
//            setImage(image: nil)
//            completion?(false)
//        }
      }

    public func SetLogoImage(_ url: String?) {
           DispatchQueue.global().async { [weak self] in
               guard let stringURL = url, let url = URL(string: stringURL) else { return }
               func setImage(image:UIImage?) {
                   DispatchQueue.main.async {
                       self?.image = image
                   }
               }
               if let data = try? Data(contentsOf: url), let image = UIImage(data: data) {
                   DispatchQueue.main.async {
                       setImage(image: image)
                   }
               } else {
                   setImage(image: nil)
               }
           }
       }
    private var activityIndicator: UIActivityIndicatorView {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.hidesWhenStopped = true
        activityIndicator.color = UIColor.white
        if #available(iOS 13.0, *) {
            activityIndicator.style =   .large
        } else {
            activityIndicator.style =   .whiteLarge
        }
        self.addSubview(activityIndicator)

        activityIndicator.translatesAutoresizingMaskIntoConstraints = false

        let centerX = NSLayoutConstraint(item: self,
                                         attribute: .centerX,
                                         relatedBy: .equal,
                                         toItem: activityIndicator,
                                         attribute: .centerX,
                                         multiplier: 1,
                                         constant: 0)
        let centerY = NSLayoutConstraint(item: self,
                                         attribute: .centerY,
                                         relatedBy: .equal,
                                         toItem: activityIndicator,
                                         attribute: .centerY,
                                         multiplier: 1,
                                         constant: 0)
        self.addConstraints([centerX, centerY])
        return activityIndicator
    }
}
public extension Notification.Name {
    static var getOcrResponse: Notification.Name {
          return .init(rawValue: "getOcrResponse")
    }
    static var taskCompletionStatus: Notification.Name {
          return .init(rawValue: "taskCompletionStatus")
    }
}
public extension UIButton {
    func loadingIndicator(_ show: Bool) {
        let indicatorTag = 808404
        if show {
            isEnabled = false
            alpha = 0
            let indicator = UIActivityIndicatorView(style: .large)
            indicator.color = .white
            indicator.center = center
            indicator.tag = indicatorTag
            superview?.addSubview(indicator)
            indicator.startAnimating()
        } else {
            isEnabled = true
            alpha = 1.0
            if let indicator = superview?.viewWithTag(indicatorTag) as? UIActivityIndicatorView {
                indicator.stopAnimating()
                indicator.removeFromSuperview()
            }
        }
    }
}
public extension UILabel {
    func loadingIndicator(_ show: Bool) {
        let indicatorTag = 808404
        if show {
            isEnabled = false
            alpha = 0
            let indicator = UIActivityIndicatorView(style: .medium)
            indicator.color = .white
            indicator.tag = indicatorTag
            let stackView = UIStackView()
            let title = UILabel()
            title.text = "Processing  "
            title.textColor = .white
            stackView.axis            = .vertical
            stackView.distribution    = .fill
            stackView.alignment       = .fill
            stackView.spacing         = 5
            stackView.addArrangedSubview(title)
            stackView.addArrangedSubview(indicator)
            superview?.addSubview(stackView)
            indicator.startAnimating()
        } else {
            isEnabled = true
            alpha = 1.0
            if let indicator = superview?.viewWithTag(indicatorTag) as? UIActivityIndicatorView {
                indicator.stopAnimating()
                indicator.removeFromSuperview()
            }
        }
    }
}

public extension UILabel {
    /// Sets the attributedText property of UILabel with an attributed string
    /// that displays the characters of the text at the given indices in subscript.
    func setAttributedTextWithSubscripts(text: String) {
        let font = self.font!
        let subscriptFont = font.withSize(font.pointSize * 0.7)
        let subscriptOffset = +font.pointSize * 0.3
        let count = text.count
        let attributedString = NSMutableAttributedString(string: text,
                                                         attributes: [.font : font])
        for index in [count-1] {
            let range = NSRange(location: index, length: 1)
            attributedString.setAttributes([.font: subscriptFont,
                                            .baselineOffset: subscriptOffset],
                                           range: range)
        }
        self.attributedText = attributedString
    }
}
public extension UITextField {
    /// Sets the attributedText property of UILabel with an attributed string
    /// that displays the characters of the text at the given indices in subscript.
    func setAttributedTextWithSubscripts(text: String) {
        let font = self.font!
        let subscriptFont = font.withSize(font.pointSize * 0.7)

        let subscriptOffset = +font.pointSize * 0.3
        let redColor = UIColor.red
        let count = text.count
        let attributedString = NSMutableAttributedString(string: text,
                                                         attributes: [.font : font])
        for index in [count-1] {
            let range = NSRange(location: index, length: 1)
            attributedString.setAttributes([.font: subscriptFont,
                                            .baselineOffset: subscriptOffset,.foregroundColor: redColor],
                                           range: range)
        }
        self.attributedText = attributedString
    }
}



