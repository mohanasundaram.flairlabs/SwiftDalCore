//
//  Components.swift
//  avkyc
//
//  Created by Admin on 09/02/22.
//

import Foundation
import UIKit



public func RenderCardView(backgroundColor:UIColor,tag:Int, shadowColor:CGColor) -> UIView {
    let view = UIView()
    view.backgroundColor =   backgroundColor
    view.layer.cornerRadius = 20.0
    view.layer.shadowColor = shadowColor
    view.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
    view.layer.shadowRadius = 6.0
    view.layer.shadowOpacity = 0.7
    view.tag = tag
    view.translatesAutoresizingMaskIntoConstraints = false
    return view
}
