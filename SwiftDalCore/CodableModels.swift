//
//  CodableModels.swift
//  app
//
//  Created by Admin on 05/10/21.
//

import Foundation

// MARK: - PheonixODScoket
public struct PheonixODModel: Codable {
    var payload: [PheonixODScokets]
}
// MARK: - Payload
public struct PheonixODScokets: Codable {
    var attr: String
    var data: PayloadData
}

// MARK: - DataClass
public struct PayloadData: Codable {
    var latitude, longitude: Double
    var altitude: String
    var accuracy: Int
    var altitudeAccuracy, heading, speed: String
    var timestamp: String
}
public struct PageSequence {
    private var page: String
    private var validationModelList: [ValidationModel]?
    public init(page: String, validationModelList: [ValidationModel]?) {
        self.page = page
        self.validationModelList = validationModelList
    }
    public func getPage() -> String {
        return self.page
    }
    public mutating func setPage(page:String) {
        self.page = page
    }
    public func getValidationModel() -> [ValidationModel]? {
        return validationModelList
    }
    public mutating func setValidationModel(validationModel:[ValidationModel]?) {
        self.validationModelList = validationModel
    }
}
// MARK: - HealthCheckModel
public struct HealthCheckModel: Codable {
    public init(referenceID: String, networkCheckID: String, statistics: Statistics) {
        self.referenceID = referenceID
        self.networkCheckID = networkCheckID
        self.statistics = statistics
    }
    
    var referenceID, networkCheckID: String
    var statistics: Statistics

    enum CodingKeys: String, CodingKey {
        case referenceID = "reference_id"
        case networkCheckID = "network_check_id"
        case statistics
    }
}

// MARK: - Statistics
public struct Statistics: Codable {
    public init(roomJoin: Bool, publish: Bool, iceConnectionState: String, webrtcStats: WebrtcStats, errors: [ErrorMessage]) {
        self.roomJoin = roomJoin
        self.publish = publish
        self.iceConnectionState = iceConnectionState
        self.webrtcStats = webrtcStats
        self.errors = errors
    }
    
    var roomJoin, publish: Bool
    var iceConnectionState: String
    var webrtcStats: WebrtcStats
    var errors: [ErrorMessage]

    enum CodingKeys: String, CodingKey {
        case roomJoin = "room_join"
        case publish
        case iceConnectionState = "ice_connection_state"
        case webrtcStats = "webrtc_stats"
        case errors
    }
}
public struct WebrtcStats: Codable {
    public init() {
    }
}
public struct ErrorMessage: Codable {
    public init(error: String, message: String) {
        self.error = error
        self.message = message
    }
    
    var error: String
    var message: String
}
public class ReadObjectFromFileModal {
    
    static func readSampleTaskDataFromFile() -> PheonixODModel? {
        let path = Bundle.main.path(forResource: "taskData", ofType: "json")
        let url = URL(fileURLWithPath: path!)
        let sportsData = try? Data(contentsOf: url)
        guard
            let data = sportsData
        else { return nil  }
        do {
            let result = try JSONDecoder().decode(PheonixODModel.self, from: data)
//            print(result)
            return result
        } catch let error {
            print("Failed to Decode Object", error)
            return nil
        }
    }
}
public class ArtifactsSets {


    public var artifactKey:String = ""
    public var taskKey:String = ""
    public var taskType:String = ""
    public var side:String = ""
    public var message:String = ""
    public var documentType:String = ""


    public init(taskKey:String,taskType:String,artifactKey:String,side:String,message:String,documentType:String) {
        self.taskType = taskType
        self.taskKey = taskKey
        self.artifactKey = artifactKey
        self.side = side
        self.message = message
        self.documentType = documentType

    }
}
