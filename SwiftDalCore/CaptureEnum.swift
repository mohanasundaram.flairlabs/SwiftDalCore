//
//  CaptureEnum.swift
//  core
//
//  Created by Admin on 18/04/22.
//

import Foundation

public enum SessionEnum {
    case SESSION_OK, SESSION_ERROR, SESSION_RECONNECTING, SESSION_CONNECTED, SESSION_DISCONNECTED
}
public enum ArtifactEnum {
    case ARTIFACT_ERROR, ARTIFACT_DIV_RESPONSE, ARTIFACT_CHECK_RESPONSE, ARTIFACT_DOC_RETRIEVAL
}
public enum PageEnum:String {
    case AVKYC = "vkyc.assisted_vkyc"
    case SVC = "self_video"
    case SKILL_SELECT = "av_skill_select"
    case CAPTURE_PREREQUISITE = "capture_prerequisite"
    case SVC_PREREQUISITE = "sv_prerequisite"
    case AV_PREREQUISITE = "av_prerequisite"
    case PAGE_END = "PAGE_END"
}
public enum ButtonTitle:String {
    case VIDEO_KYC = "START VIDEO KYC"
    case SELF_VIDEO = "START SELF VIDEO"
    case PROCEED = "PROCEED"
    case INIT_VERIFICATION = "INITIATE VERIFICATION"
}
