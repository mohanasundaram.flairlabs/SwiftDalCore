//
//  ICaptureCallback.swift
//  core
//
//  Created by Admin on 18/04/22.
//

import Foundation

public protocol ICaptureCallback {
    func onSocketOpen()
    func onSocketError()
    func onSessionReceive(event:SessionEnum,payload:NSObject)
    func onArtifactReceive(event:ArtifactEnum,payload:NSObject)
}
