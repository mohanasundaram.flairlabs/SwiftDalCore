//
//  ThemeConfig.swift
//  app
//
//  Created by Admin on 17/12/21.
//

import Foundation
public class ThemeConfig {
    //TODO: Add default values
    private var headerBackgroundColor: String? = "#ffffff"
    private var headerTextColor: String? = "#000000"
    private var isDisplayFooter: Bool = false
    private var secondaryMainColor: String? = "#ffffff"
    private var secondaryContrastColor: String? = "#000000"
    private var primaryMainColorString: String? = "#279a9e"
    private var primaryContrastColorString: String? = "#ffffff"
    private var logo: String? = "https://blog.idfy.com/content/images/size/w1000/2018/07/idfy_logo_600-600-1.png"
    private var primaryFont:String?
    private var secondaryFont:String?

    private static var privateSharedInstance: ThemeConfig?

    public static var shared: ThemeConfig = {

        if privateSharedInstance    == nil {
            privateSharedInstance = ThemeConfig()
        }

        return privateSharedInstance!
    }()
    
    required init() {
        
    }
    
    public func getHeaderBackgroundColor() -> String {
        return headerBackgroundColor ?? ""
    }
    public func setHeaderBackgroundColor(headerBackgroundColor: String) {
        self.headerBackgroundColor = headerBackgroundColor
    }
    public func getHeaderTextColor() -> String {
        return headerTextColor ?? ""
    }
    public func setHeaderTextColor(headerTextColor: String) {
        self.headerTextColor = headerTextColor
    }
    public func getDisplayFooter() -> Bool {
        return self.isDisplayFooter
    }
    public func setDisplayFooter(displayFooter: Bool) {
        self.isDisplayFooter = displayFooter
    }
    public func getSecondaryMainColor() -> String {
        return secondaryMainColor ?? ""
    }
    public func setSecondaryMainColor(secondaryMainColor: String) {
        self.secondaryMainColor = secondaryMainColor
    }
    public func getSecondaryContrastColor() -> String {
        return secondaryContrastColor ?? ""
    }
    public func setSecondaryContrastColor(secondaryContrastColor: String) {
        self.secondaryContrastColor = secondaryContrastColor
    }
    public func getPrimaryMainColor() -> String {
        return primaryMainColorString ?? ""
    }
    public func setPrimaryMainColor(primaryMainColor: String) {
        self.primaryMainColorString = primaryMainColor
    }
    public func getPrimaryContrastColor() -> String {
        return primaryContrastColorString ?? ""
    }
    public func setPrimaryContrastColor(primaryContrastColor: String) {
        self.primaryContrastColorString = primaryContrastColor
    }
    public func getLogo() -> String {
        return logo ?? ""
    }
    public func setLogo(logo: String) {
        self.logo = logo
    }
    public func setPrimaryFont(primaryFont:String) {
        self.primaryFont = primaryFont
    }
    public func getPrimaryFont() -> String {
        return self.primaryFont ?? ""
    }
    public func setSecondaryFont(secondaryFont:String) {
        self.secondaryFont = secondaryFont
    }
    public func getSecondaryFont() -> String {
        return self.secondaryFont ?? ""
    }
    public func setThemeConfig(themeObject:NSObject) {
        let customHeader = themeObject.value(forKey: "custom_header") as! NSObject
        self.setHeaderBackgroundColor(headerBackgroundColor: customHeader.value(forKey: "backgroundColor") as? String ?? "")
        if customHeader.value(forKey: "backgroundColor") as? String == "#fff" {
            self.setHeaderBackgroundColor(headerBackgroundColor: "#ffffff")
        }
        self.setHeaderTextColor(headerTextColor: customHeader.value(forKey: "color") as? String ?? "")
        if customHeader.value(forKey: "color") as? String ?? "" == "#000" {
            self.setHeaderTextColor(headerTextColor: "#000000")
        }
        let footerObject = themeObject.value(forKey: "custom_footer") as? NSObject
        if footerObject != nil {
            self.setDisplayFooter(displayFooter: footerObject?.value(forKey: "display") as! Bool)
        }
        let paletteObject = themeObject.value(forKey: "palette") as? NSObject
        if paletteObject != nil {
            let primaryObject = paletteObject?.value(forKey: "secondary") as? NSObject
            if primaryObject != nil {
                self.setSecondaryMainColor(secondaryMainColor: primaryObject?.value(forKey: "main") as? String ?? "")
                self.setSecondaryContrastColor(secondaryContrastColor: primaryObject?.value(forKey: "contrastText") as? String ?? "")
            }
            let primaryPaletteObject = paletteObject?.value(forKey: "primary") as? NSObject
            if primaryPaletteObject != nil {
                self.setPrimaryMainColor(primaryMainColor: primaryPaletteObject?.value(forKey: "dark") as? String ?? "")
                self.setPrimaryContrastColor(primaryContrastColor: primaryPaletteObject?.value(forKey: "contrastText") as? String ?? "")
            }
        }
        let typography = themeObject.value(forKey: "typography") as? NSObject
        if typography != nil {
            let fontFamily = typography?.value(forKey: "fontFamily") as? NSObject
            if fontFamily != nil {
                self.setPrimaryFont(primaryFont: fontFamily?.value(forKey: "primary") as? String ?? "")
                self.setSecondaryFont(secondaryFont: fontFamily?.value(forKey: "secondary") as? String ?? "")
            }
        }
    }
}
