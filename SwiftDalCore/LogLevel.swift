//
//  LogLevel.swift
//  app
//
//  Created by Admin on 30/11/21.
//

import Foundation

public struct LogLevel: Codable {
  
    public static let shared = LogLevel()
    
    public var Info: String = "info"
    public var Error: String = "error"
    public var Warning: String = "warn"
}
