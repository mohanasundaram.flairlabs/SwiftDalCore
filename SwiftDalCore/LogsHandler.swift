//
//  LogsHandler.swift
//  core
//
//  Created by Admin on 03/02/22.
//

import Foundation
import UIKit
import CoreData
import EventKit
import EventKitUI

public class LogsHandler {
    private var RETRY_COUNT_LIMIT: Int = 25

    private var isRetrying: Bool = false
    private var isDbEmpty: Bool = false
    private var remainingRequests: Int = 0
    var dB: NSManagedObjectContext?
    var array: [Any]?

    public init() {
        self.isRetrying = false
        self.isDbEmpty = false
    }
    func addEntry(entry: [String: Any]) {

        dB    =   DatabaseManagerSingleton.sharedInstance.managedObjectContextValue

//        if let loggerEvent   =   NSEntityDescription.insertNewObject(forEntityName: "LoggerDatabase", into: dB!) as? LoggerDatabase {
//            loggerEvent.logger = entry as NSObject
//            try? dB?.save()
//        }
    }

    public func retryEntries(dalCapture: DALCapture, token: String, loggerUrl: String) {
        if !isRetrying && !isDbEmpty && dB != nil {
            isRetrying = true

            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "LoggerDatabase")

            do {
                array = try dB?.fetch(fetchRequest)
            } catch {
                print("ERROR")
            }
            remainingRequests = array!.count
            for data in array as! [NSObject] {
                sendLogs(dalCapture: dalCapture, logs: data, token: token, loggerUrl: loggerUrl)
            }
        } else {
            isRetrying = false
            isDbEmpty = true
        }
    }
    private func sendLogs(dalCapture: DALCapture, logs: NSObject, token: String, loggerUrl: String) {
//        dalCapture.logger(logs: logs, url: loggerUrl) { onSessionSuccess, _ in
//            if onSessionSuccess != nil {
//                self.array?.removeFirst()
//                self.stopRetry()
//            } else {
//                self.stopRetry()
//            }
//        }
    }
    private func stopRetry() {
        if remainingRequests == 0 {
            isRetrying = false
        } else {
            remainingRequests = remainingRequests - 1
        }

    }
    public func clearEntries() {
        dB?.reset()
    }
}
//extension LoggerDatabase {
//    class func fetchRequest() -> NSFetchRequest<LoggerDatabase>? {
//        return NSFetchRequest(entityName: "LoggerDatabase")
//    }
//}
