////
////  SocketConfigure.swift
////  app
////
////  Created by Admin on 25/08/21.
////
//
//import UIKit
//import Foundation
//import Alamofire
//import SwiftPhoenixClient
//import Starscream
//protocol SocketConnectionCallBack: AnyObject {
//    func initSocket(socket: Socket)
//    func initChannel(channel: Channel)
//    func onSocketError(message: String)
//    func onSocketDisconnected()
//    func onSocketOpen()
//}
//public class SocketConfigure: NSObject {
//
//    internal init(webSocketUrl: String = "", channel: Channel? = nil, socket: Socket? = nil) {
//        self.webSocketUrl = webSocketUrl
//        self.channel = channel
//        self.socket = socket
//    }
//
//    static let shared = SocketConfigure()
//
//    private var webSocketUrl: String = ""
//    private var channel: Channel?
//    var socket: Socket?
//
//    private var pgLogger: Logger?
//
//    func initialise(webSocketUrl: String, logger: Logger?) {
//        self.webSocketUrl = webSocketUrl
//        self.pgLogger = logger
//    }
//
//    public func setUpSocket(completion:@escaping(_ socket: Socket?, _ failure: String?) -> Void) {
//        let socket = Socket(webSocketUrl)
//        socket.timeout = 60.0
//        socket.heartbeatInterval = 10.0
//            socket.onError { [self] error in
//                print("socket connection error: ", error)
//                completion(nil, "Socket Connection Error")
//
//                var logs = ModalLogDetails()
//                logs.service = "SocketConnect"
//                logs.event_name = "\(error)"
//                logs.event_type = "OnError"
//                logs.event_source = "setupSocket"
//
//                pgLogger?.log(logLevel: LogLevel.shared.Info, logDetails: logs, meta: [:])
//            }
//            socket.onClose { [self] in
//                var logs = ModalLogDetails()
//                logs.service = "SocketConnect"
//                logs.event_name = "Close"
//                logs.event_type = "Closed"
//                logs.event_source = "setupSocket"
//
//                pgLogger?.log(logLevel: LogLevel.shared.Info, logDetails: logs, meta: [:])
//                print("socket connection closed")
//                completion(nil, "Socket Connection Error")
//            }
//            socket.onOpen { [self] in
//                var logs = ModalLogDetails()
//                logs.service = "SocketConnect"
//                logs.event_name = "\(webSocketUrl)"
//                logs.event_type = "Invoked"
//                logs.event_source = "setupSocket"
//
//                pgLogger?.log(logLevel: LogLevel.shared.Info, logDetails: logs, meta: [:])
//
//                print("socket connection: opened")
//            }
//            socket.connect()
//        completion(socket, nil)
//    }
//    func setupChannel(socket: Socket, topic: String, payload: Payload, completion:@escaping(_ channel: Channel?, _ failure: String?) -> Void) {
//        channel = socket.channel(topic, params: payload)
//        if channel != nil {
//            completion(channel, nil)
//        }
//    }
//
//}
